# Docker Compose

## Compose YAML
* Description of a whole system
* Containers are abstracted toServices.
* Services must have an image to start from
* All other parameters (networks to join, environmental variables, ports etc.) can be set by objects
* Volumes and networks can be defined too
* Name of the objects are appearing in ls, psand inspectcommands!
* Name of services become DNS names!
* It has different versions. The latest version is 3.8 which is supported by Docker 19.03

## An example of a Compose YAML file
![](img/compose.png)

## Compose can build images
* In the Service definitions, a build process can be described
* It runs only if the required image for the service is not available (can be forced to build anyway)
* Dockerfile path and build context is distinguished: it allows to organize code in repositories better

## Compose CLI
* Using Compose YAML file, only two commands needed to start and clean up all the resources:
* docker-compose up/down
* these commands starts and removes all the resources described in the docker-compose.yml file in the current folder (or specified with the –fflag)