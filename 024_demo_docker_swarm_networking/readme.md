## Initialize swarm

```bash
docker swarm init --advertise-addr eth0
Swarm initialized: current node (pag1krz0twsf0dg8k49i520s1) is now a manager.

To add a worker to this swarm, run the following command:

    docker swarm join --token SWMTKN-1-4qe500ho7ppsljy4um3oxotatyevt3sisf0495w4kk6pnv86eq-8lki9qfgs5qzhvhife4qum88a 192.168.0.13:2377

To add a manager to this swarm, run 'docker swarm join-token manager' and follow the instructions.
```

## Add worker nodes to the cluster
Now ssh into other machines that you would like to add as worker nodes and execut the command that was given to you by the previous **docker swarm init** run.


```bash
docker swarm join --token SWMTKN-1-4qe500ho7ppsljy4um3oxotatyevt3sisf0495w4kk6pnv86eq-8lki9qfgs5qzhvhife4qum88a 192.168.0.13:2377
```

If you check your node list now on eaither of the machines you will se that we successfully built a cluster of machines with docker swarm.


```bash
docker node ls
ID                            HOSTNAME            STATUS              AVAILABILITY        MANAGER STATUS      ENGINE VERSION
pag1krz0twsf0dg8k49i520s1 *   node1               Ready               Active              Leader              19.03.11
6hcyfpdhdenqr9er696eezf74     node2               Ready               Active                                  19.03.11
```

In case you need to leave the cluster:

```bash
docker swarm leave
```

## deploy application with database (drupal)

```bash
docker network create --driver overlay mydrupal
```

```bash
docker network ls
```

```bash
docker service create --name psql --network mydrupal -e POSTGRES_PASSWORD=mypass postgres
```

```bash
docker service ls
```

```bash
docker service ps psql
```

```bash
docker container logs psql TAB COMPLETION
```

```bash
docker service create --name drupal --network mydrupal -p 80:80 drupal
```

```bash
docker service ls
```

```bash
watch docker service ls
```

```bash
docker service ps drupal
```

```bash
docker service inspect drupal
```
