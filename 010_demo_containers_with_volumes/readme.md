## Now try this!
[Docker Playground ](https://labs.play-with-docker.com/)

## Let's create mysql container

```bash
docker pull mysql
```
Might download a new version of mysql and output something like this:
```bash
Using default tag: latest
latest: Pulling from library/mysql
bb79b6b2107f: Pull complete 
...
49e112c55976: Pull complete 
Digest: sha256:8c17271df53ee3b843d6e16d46cff13f22c9c04d6982eb15a9a47bd5c9ac7e2d
Status: Downloaded newer image for mysql:latest
docker.io/library/mysql:latest
```

You can check the details of the image with inspect command:

```bash
docker image inspect mysql
```

Now create an instance:
```bash
docker container run -d --name mysql -e MYSQL_ALLOW_EMPTY_PASSWORD=True mysql
```
Where `-e` option allows you to pass environment variables to the containers and `--name xyz` can be used to name this container `xyz`. In case you name your container you can use this name to reference the container by this name in each command.

```bash
docker container ls
```

As you can see the container has a name:
```bash 
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                 NAMES
d7272afe1818        mysql               "docker-entrypoint.s…"   2 seconds ago       Up 1 second         3306/tcp, 33060/tcp   mysql
```

You can also instpect the conatiners
```bash
docker container inspect mysql
```
Which will yeald a long json structure with all the current information about the container.

## Where does mysql store database files?

```bash
docker volume ls
```

You will see that there is a new volume created by mysql container.

```bash
DRIVER              VOLUME NAME
local               7adfbd05569a45543d7295bb252633bc16778f5a99e5567aa872fd151bc76a65
```

This is a persistent volume. This type of volume will not be deleted together with the container. You will need to force delete it.

Why does mysql create this? Because the mysql Dockerimage contains a **VOLUME** command. Unless we provdie a volume when creating the container, docker will create a persistent storage for the conainer.

You can create a persistent storage:
```bash
docker vollume create db-data
```
Now list all the available persistent storages
```bash
docker volume ls
```
See that we have now our own storage called db-data
```bash
DRIVER              VOLUME NAME
local               7adfbd05569a45543d7295bb252633bc16778f5a99e5567aa872fd151bc76a65
local               db-data
```
Use this storage when create a new mysql2 container:
```bash
docker container run -d --name mysql2 -e MYSQL_ALLOW_EMPTY_PASSWORD=True -v db-data:/var/lib/mysql mysql
```
If you list all your containers
```bash
docker container ls
```
You'll see a new mysql2 container
```bash
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                 NAMES
c78b2d5d3259        mysql               "docker-entrypoint.s…"   2 minutes ago       Up 2 minutes        3306/tcp, 33060/tcp   mysql2
d7272afe1818        mysql               "docker-entrypoint.s…"   41 minutes ago      Up 41 minutes       3306/tcp, 33060/tcp   mysql
```

But if you check the list of volumes
```bash
docker volume ls
```
you'll see that we don't have a new volume:
```bash
DRIVER              VOLUME NAME
local               7adfbd05569a45543d7295bb252633bc16778f5a99e5567aa872fd151bc76a65
local               db-data
```

## You can also use volume binding 

Create a folder and cd into it.
Create an index.html with contnet:
```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>My worked!</h1>
</body>
</html>
```

Use local directory to mount into the container
```bash
docker container run -d --name nginx -p 80:80 -v $(pwd):/usr/share/nginx/html nginx
```