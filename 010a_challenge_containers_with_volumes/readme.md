## Your challnege if you accept it!
1. start a Jekyll site with docker (hint: https://hub.docker.com/r/bretfisher/jekyll-serve/ )
1. use port binding with 80:4000
1. use volume binding and bind the current directory to `/site`
1. try changing the site content while the container runs.
