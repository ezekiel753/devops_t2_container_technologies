# Container Technologies
This repository contains the complete workshop of DevOps Conteriner Technologies module.
It is advised to follow the content as indicated by the index number in the folder name.
For example content of folder staring with `009_...` should be processed before content 
folder starting with `011_...`.

## Theory and Demos
These folders contain portions of the theoratical matherial and demonstrations.

## Challenges
Challenges are excercises that are meant to be processed by each student on their own.
Each challenge indicateds the aproximate time required to complete it so that you can plan
ahead if you have the necessary uninterrupted time to complete the whole challenge.

Ideally challenges are done duringe the workshop days so that you can ask help from the 
mentor who is present on the workshop. If you don't have time to participate during the 
workshop you can of course do this at another point in time suitable for you. In case you
do these chellenges outside of the official workshop hours please do not hasitate reaching outside
to the mentor via e-mail and ask help even after the workshop.

## Homework
Homeworks are meant to be done after the workshop. These are similar exercises to the completed
challenges, but are mixed up a bit in order to give you a chance to try out the leaned technologies/techniques.

## Recordings
You will have access to the recording of the workshop activities. These recordings will be shared with you later on this page.

## Knowledge Check Quiz
You will receive a link during the week of the workshops to participate in a knowdeldge check quiz.
Please participage to get feedback about how well you learned the material of this week.

## Feedback
Please provide feedback via this form after the workshop days:

1. Workshop day 1 feedback form:
1. Workshop day 2 feedback form: 

## Prerequisites
You should have 
* GitLab account
* Docker Hub account

## Tools used throughout the workshop demos, challenges and homework assingments
* Docker playground: https://labs.play-with-docker.com/
* Kubernetes playground: https://labs.play-with-k8s.com/
* Katacoda: katacoda.com