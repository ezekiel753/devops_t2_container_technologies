## Initialize swarm
We assume that you have your swarm up and running. 

## Create overlay network
In order to do so use **`docker network create`** command
```bash
docker network create \
--driver overlay \
--subnet 10.0.0.0/12 \
--opt encrypted \
services
```
You can create a new container and right away attach it to the network of your choice by useing the 
**`--network`** option of **`docker service create`** command

(Actually you can also use networks in plain docker containers in a similar fashion:
**`--network`** option of **`docker container run`** command)
```bash
docker service create \
--replicas 2 \
--name nginx \
--network services \
--publish 80:80 \
nginx
```
Then check your services
```bash
docker service ls
```