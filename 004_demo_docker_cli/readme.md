## Time to meet our main Docker Sandbox
[Docker Playground ](https://labs.play-with-docker.com/)

![](https://www.docker.com/sites/default/files/Whale%20Logo332_5.png)

You **must have** a dockerhub.com account to use this online playground.

Open a new session to [Docker Playground ](https://labs.play-with-docker.com/).

You have ~ 4 hours of free work time before your session will be deleted.

![](img/session_timeout.png)

You can create multiple nodes if you'd like
![](img/add_new_instance_button.png)

```
Now add one node with the ADD NEW INSTANCE BUTTON
```

This is how your workspace should look like with one node:
![](img/your_playground_is_ready.png)

## Deploying nginx web server

```bash
docker image pull nginx
```

You should see a similar output:
```
Using default tag: latest
latest: Pulling from library/nginx
bb79b6b2107f: Pull complete 
111447d5894d: Pull complete 
a95689b8e6cb: Pull complete 
1a0022e444c2: Pull complete 
32b7488a3833: Pull complete 
Digest: sha256:ed7f815851b5299f616220a63edac69a4cc200e7f536a56e421988da82e44ed8
Status: Downloaded newer image for nginx:latest
docker.io/library/nginx:latest
```

Now let's run the following command:

```bash
docker container run -d -p 80:80 nginx
```

You should see a contianer identifier as an output:
```bash
9d0e690a3444571864dae72334c9a9a7e934939a4dfcf0e5bfbc60367ea0f10b
```

You should see that a new port `80` link appeard.

![](img/open_port.png)

Click this link to open a new tab where the output from the container will be visible

![](img/welcome_to_nginx.png)

Let's check the running containers

```bash
docker container ps
```

should output something similar
```bash
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                NAMES
9d0e690a3444        nginx               "/docker-entrypoint.…"   12 minutes ago      Up 12 minutes       0.0.0.0:80->80/tcp   zealous_mestorf
```

Let's check the logs of the container
```bash
docker conainer logs <container_id>
```

should output something similar
```bash
/docker-entrypoint.sh: /docker-entrypoint.d/ is not empty, will attempt to perform configuration
/docker-entrypoint.sh: Looking for shell scripts in /docker-entrypoint.d/
/docker-entrypoint.sh: Launching /docker-entrypoint.d/10-listen-on-ipv6-by-default.sh
10-listen-on-ipv6-by-default.sh: Getting the checksum of /etc/nginx/conf.d/default.conf
10-listen-on-ipv6-by-default.sh: Enabled listen on IPv6 in /etc/nginx/conf.d/default.conf
/docker-entrypoint.sh: Launching /docker-entrypoint.d/20-envsubst-on-templates.sh
/docker-entrypoint.sh: Configuration complete; ready for start up
172.18.0.1 - - [26/Oct/2020:06:57:07 +0000] "GET / HTTP/1.1" 200 612 "-" "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36" "-"
2020/10/26 06:57:07 [error] 29#29: *1 open() "/usr/share/nginx/html/favicon.ico" failed (2: No such file or directory), client: 172.18.0.1, server: localhost, request: "GET /favicon.ico HTTP/1.1", host: "ip172-18-0-22-bub6ut9qckh0009abv70-80.direct.labs.play-with-docker.com", referrer: "http://ip172-18-0-22-bub6ut9qckh0009abv70-80.direct.labs.play-with-docker.com/"
172.18.0.1 - - [26/Oct/2020:06:57:07 +0000] "GET /favicon.ico HTTP/1.1" 404 555 "http://ip172-18-0-22-bub6ut9qckh0009abv70-80.direct.labs.play-with-docker.com/" "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36" "-"
```
