# Networking
How multiple conrainers can be connected to eachother?

## What happens in

```bash 
docker container run –p 80:80 nginx
```

* Looks for that image locally in image cache, doesn't find anything
* Then looks in remote image repository (defaults to Docker Hub)
* Downloads the latest version (nginx:latest by default)
* Creates new container based on that image and prepares to start
* Gives it a virtual IP on a private network inside docker engine
* Opens up port 80 on host and forwards to port 80 in container
* Starts container by using the CMD in the image Dockerfile


## Docker Networks Defaults
* Each container connected to a private virtual network "bridge"
* Each virtual network routes through NAT firewall on host IP
* All containers on a virtual network can talk to each other
without -p
* Best practice is to create a new virtual network for each app:
* `network "my_web_app"` for mysql and php/apache containers
* `network "my_api"` for mongo and nodejs containers

## "Batteries Included, But Removable"
* Defaults work well in many cases, but easy to swap out parts to
customize it
* Make new virtual networks
* Attach containers to more then one virtual network (or none)
* Skip virtual networks and use host IP (`--net=host`)
* Use different Docker network drivers to gain new abilities
* and much more...

## Show networks docker network ls
* Inspect a network `docker network inspect`
* Create a network `docker network create --driver`
* Attach a network to container `docker network connect`
* Detach a network from container `docker network disconnect`

## Docker Networks: Default Security
* Create your apps so frontend/backend sit on same Docker
network
* Their inter-communication never leaves host
* All externally exposed ports closed by default
* You must manually expose via -p, which is better default
security!
* This gets even better later with Swarm and Overlay networks

## Docker Networks: DNS
* Understand how DNS is the key to easy inter-container comms
* See how it works by default with custom networks
* Containers shouldn't rely on IP's for inter-communication
* **DNS** for friendly names is built-in if you use custom networks
* This gets way easier with Docker Compose in future Section