## Your challnege if you accept it!
Create your own Nginx web server image and deploy a container based on your image.
1. Follow the same procedure we did with apache but this time use the nginx image as FROM in your Dockerfile.
1. Make sure to run the docker command with the `-d` option to keep it running in the background
1. Make sure to open the `port 80` from the host operating system by using the option `-p 80:80` 
1. Try to clean  up after yourself (kill and remove the container)


## Approximate time to complete the challenge
You will need 5-10 min of uninterrupted time to complete this challenge.

Exatra challnege:
## Assignment: Manage Multiple Containers
- docs.docker.com and --help are your friend
- Run a nginx, a mysql, and a httpd (apache) server
- Run all of them --detach (or -d), name them with --name
- nginx should listen on 80:80, httpd on 8080:80, mysql on 3306
- When running mysql, use the --env option (or -e) to pass in
    MYSQL_RANDOM_ROOT_PASSWORD=yes
- Use docker container logs on mysql to find the random password
    it created on startup
- Clean it all up with docker container stop and docker
    container rm (both can accept multiple names or ID's)
- Use docker container ls to ensure everything is correct before
    and after cleanup